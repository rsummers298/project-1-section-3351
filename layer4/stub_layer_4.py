# This is a stub for Layer 4.

# Import Layers
import sys
sys.path.append('../')
import layer3.stub_layer_3 as Layer3
import logging

#import time so we can use sleep
import time

class StubLayer4:
    #initialize local variables
    dest_port = ""
    dp_cbf = dict()
    ACKed = 0

    def __init__(self):
        """Create a Layer 4 Object.

            This initializer doesn't ask for a callback since there
            will likely be multiple callbacks connected to sockets.
            Use the `connect_to_socket` method to register a
            layer_5_cb.

        """
        
        # Connect to Layer 3
#----- CHANGE STUBLAYER3 TO MATCH WHATEVER GROUP3 NAMED THEIR FILE
        self.layer3 = Layer3.StubLayer3(self.from_layer_3)

    def connect_to_socket(self, port_number, layer_5_cb):
        """Connect an application to a socket.

        `port_number` - Integer.

        `layer_5_cb` - Function. This function will be called when data comes in on the specified port.
        """
        
        #save destination port and callback functions into a python dictionary so we
        #can multiplex. Save this callback function as a local variable
        self.dp_cbf[str(port_number)] = layer_5_cb
#test        self.layer_5_cb = layer_5_cb
        
        logging.debug(f"Connected to port {port_number}")


    def from_layer_5(self, data, src_port, dest_port, dest_addr):
        """Call this function to send data to this layer from layer 5

        `src_port` - Integer. This is the port number on this machine
        `dest_port` - Integer. This is the port number on the
                      receiver's machine.
        """
        logging.debug(f"Layer 4 received msg from Layer 5: {data}")
        
        #make the message to pass down to layer3
        dest_port = str(dest_port)
        header = dest_port.zfill(4)
        message = header + data
        
        # Pass the message down to Layer 3 and wait for ACK
        #resend every 5 seconds if it doesn't get ACKed
        i = 0
        self.layer3.from_layer_4(message)        
        while(self.ACKed == 0):
            if(i > 50):
                self.layer3.from_layer_4(message)
                i = 0
            time.sleep(0.1)

    def from_layer_3(self, data):
        """Call this function to send data to this layer from layer
        3
        """
        logging.debug(f"Layer 4 received msg from Layer 3: {data}")

        #Determine if this is an ACK or not. If so, get rid of the 1 header
        ACK = 0
        if(data[0] == "1"): 
            ACK = 1
            data = data[1:]

        #parse the message for dest_port and message
        #use that information to find callback function
        message = data[4:]
        header = data[:4]
        dest_port = header.lstrip('0')
        callback = self.dp_cbf[str(dest_port)]

        #if this is an ACK, change the local variable to show it's ACKed
        #if not, send an ACK for this message and pass it up to layer 5
        if(ACK == 1):   
            self.ACKed = 1
        elif(ACK == 0): 
            self.layer3.from_layer_4("1" + header + message)
            callback(message)
