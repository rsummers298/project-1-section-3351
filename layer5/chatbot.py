from tkinter import *

# GUI
root = Tk()
root.title("Chatbot")
 
BG_GRAY = "#ABB2B9"
BG_COLOR = "#17202A"
TEXT_COLOR = "#EAECEE"

FONT = "Helvetica 14"
FONT_BOLD = "Helvetica 13 bold"

active = False

# Send function
def send():
    global active 
    send = "You -> " + e.get()
    txt.insert(END, "\n" + send)

    user = e.get().lower()
    
    if (user == "hello"):
        txt.insert(END, "\n" + "Weapon Controller -> Hi there, how can I help?")
    
    elif(user == "status" and active):
        txt.insert(END, "\n" + "Weapon Controller -> Ready")
    
    elif(user == "status" and not active):
        txt.insert(END, "\n" + "Weapon Controller -> Sleeping")
    
    elif(user == "activate"):
        txt.insert(END, "\n" + "Weapon Controller -> Activating")
        active=True

    elif(user == "fire"):
        txt.insert(END, "\n" + "Weapon Controller -> Firing Weapon!")
        active=False

    else:
        txt.insert(END, "\n" + "Weapon Controller -> Sorry! I didn't understand that")
    
    e.delete(0, END)

lable1 = Label(root, bg=BG_COLOR, fg=TEXT_COLOR, text="Weapons Controller", font=FONT_BOLD, pady=15, width=20, height=1).grid(row=0)

txt = Text(root, bg=BG_COLOR, fg=TEXT_COLOR, font=FONT, width=60)
txt.grid(row=1, column=0, columnspan=2)

scrollbar = Scrollbar(txt)
scrollbar.place(relheight=1, relx=0.974)

e = Entry(root, bg="#2C3E50", fg=TEXT_COLOR, font=FONT, width=55)
e.grid(row=2, column=0)

send = Button(root, text="Send", font=FONT_BOLD, bg=BG_GRAY,command=send).grid(row=2, column=1)
 
root.mainloop()

# need a way to send and recieve data from weapons controller 
