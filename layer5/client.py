from tkinter import *
from tkinter import font
from tkinter import ttk
import hashlib
import threading
import rsa 
import base64

PORT = 12345

# added global variables 
username=""
pword=""
presUser="791301f9aa89552b0e68e6670d80cd25" # President
presPword="92c50217990d241f716496f34ad78a7e" # Section 3351 is the best!
opsUser="e847897826ceb8346eb5141f8c23436a" # ops
opsPword="6d4dfeb3e11e25c8c1a00d9d4f1e02bb" # Networking is awesome!
(public_key, private_key) = rsa.newkeys(512)

class GUI:
    def __init__(self):
        # added global variables 
        global username
        global pword
        # chat window which is currently hidden
        self.Window = Tk()
        self.Window.withdraw()
        # login window
        self.login = Toplevel()
        # set the title
        self.login.title("Login")
        self.login.resizable(width=False,
                             height=False)
        self.login.configure(width=400,
                             height=300)
        # create a Label
        self.pls = Label(self.login,
                         text="Login to Continue",
                         justify=CENTER,
                         font="Helvetica 14 bold")

        self.pls.place(relheight=0.15,
                       relx=0.2,
                       rely=0.07)
        # create a Label
        self.labelName = Label(self.login,
                               text="Name: ",
                               font="Helvetica 12")
        self.labelName.place(relheight=0.2,
                             relx=0.1,
                             rely=0.2)
        # create a entry box for
        # typing the message
        self.entryName = Entry(self.login,
                               font="Helvetica 14")
        username = self.entryName.get()
        self.entryName.place(relwidth=0.4,
                             relheight=0.12,
                             relx=0.35,
                             rely=0.2)
        # set the focus of the cursor
        self.entryName.focus()
        # create password box
        self.labelPassword = Label(self.login,
                                    text="Password: ",
                                    font="Helvetica 12")
        self.labelPassword.place(relheight=0.2,
                             relx=0.1,
                             rely=0.35)
        self.entryPassword = Entry(self.login,
                                    font="Helvetica 14", 
                                    show='*')
        pword=self.entryPassword.get()
        self.entryPassword.place(relwidth=0.4,
                             relheight=0.12,
                             relx=0.40,
                             rely=0.35) 
        self.entryPassword.focus()
        # create a Continue Button
        # along with action
        self.go = Button(self.login,
                         text="CONTINUE",
                         font="Helvetica 14 bold",
                         command=lambda: self.goAhead(self.entryName.get()))
        self.go.place(relx=0.5,
                      rely=0.6, anchor='center')
        self.Window.mainloop()

    def goAhead(self, username):
        if(self.verify()):
            self.login.destroy()
            self.layout(username)
            # the thread to receive messages
            rcv = threading.Thread(target=self.receive)
            rcv.start()
        # probs should figure out error message (could recall __init__ and destroy login but dont start other thread)

    # function to verify users and passwords
    def verify(self):
        user=hashlib.md5(username.encode())
        password=hashlib.md5(pword.encode())
        if(user.hexdigest()==presUser or user.hexdigest()==opsUser):
            if(password.hexdigest()==presPword or password.hexdigest()==opsPword):
                return True
        else: 
            return False
    
    # The main layout of the chat
    def layout(self, name):
        self.name = name

        # to show chat window
        self.Window.deiconify()

        self.Window.title("CHATROOM")

        self.Window.resizable(width=False,
                              height=False)

        self.Window.configure(width=470,
                              height=550,
                              bg="#17202A")

        self.labelHead = Label(self.Window,
                               bg="#17202A",
                               fg="#EAECEE",
                               text=self.name,
                               font="Helvetica 13 bold",
                               pady=5)

        self.labelHead.place(relwidth=1)

        self.line = Label(self.Window,
                          width=450,
                          bg="#ABB2B9")
 
        self.line.place(relwidth=1,
                        rely=0.07,
                        relheight=0.012)
 
        self.textCons = Text(self.Window,
                             width=20,
                             height=2,
                             bg="#17202A",
                             fg="#EAECEE",
                             font="Helvetica 14",
                             padx=5,
                             pady=5)

        self.textCons.place(relheight=0.745,
                            relwidth=1,
                            rely=0.08)
 
        self.labelBottom = Label(self.Window,
                                 bg="#ABB2B9",
                                 height=80)

        self.labelBottom.place(relwidth=1,
                               rely=0.825)

        self.entryMsg = Entry(self.labelBottom,
                              bg="#2C3E50",
                              fg="#EAECEE",
                              font="Helvetica 13")

        # place the given widget
        # into the gui window
        self.entryMsg.place(relwidth=0.74,
                            relheight=0.06,
                            rely=0.008,
                            relx=0.011)
 
        self.entryMsg.focus()

        # need a destination label + entry for destination 
        self.labelDest = Label(self.Window,
                                bg="#ABB2B9",
                                height=80)
        
        self.labelDest.place(relwidth=1,
                            rely=1)
        
        self.entryDest = Entry(self.labelDest,
                                bg="#2C3E50",
                                fg="#EAECEE",
                                font="Helvetica 13")
        
        self.entryDest.place(relwidth=0.74,
                            relheight=0.06,
                            rely=0.010,
                            relx=0.011)
        
        self.entryDest.focus()
        
        # create a Send Button
        self.buttonMsg = Button(self.labelBottom,
                                text="Send",
                                font="Helvetica 10 bold",
                                width=20,
                                bg="#ABB2B9",
                                command=lambda: self.sendButton(self.entryMsg.get(),self.entryDest.get()))

        self.buttonMsg.place(relx=0.77,
                            rely=0.008,
                            relheight=0.06,
                            relwidth=0.22)

        # determine if destination was inputed by user 
        if(self.entryDest.get()==""):
            self.buttonMsg['state'] = DISABLED 
            # should probably show error statement 
        else:
            self.buttonMsg['state'] = NORMAL
 
        self.textCons.config(cursor="arrow")

        # create a scroll bar
        scrollbar = Scrollbar(self.textCons)
 
        # place the scroll bar
        # into the gui window
        scrollbar.place(relheight=1,
                        relx=0.974)
 
        scrollbar.config(command=self.textCons.yview)
 
        self.textCons.config(state=DISABLED)
    # function to basically start the thread for sending messages

    def sendButton(self, msg, dest):
        self.textCons.config(state=DISABLED)
        self.msg = msg
        self.dest= dest
        self.entryMsg.delete(0, END)
        self.entryDest.delete(0,END)
        snd = threading.Thread(target=self.sendMessage)
        snd.start()
    
    # encyrption function
    def encrypt(message, public_key):
        """Encrypts a message using RSA.

        Returns a UTF-8 string.

        message - UTF-8 String.
        public_key - rsa.PublicKey.
        """
        message_bytes = message.encode("UTF-8")
        encrypted_bytes = rsa.encrypt(message_bytes, public_key)
        base64_bytes = base64.b64encode(encrypted_bytes)
        base64_str = base64_bytes.decode("UTF-8")
        
        return base64_str

    def decrypt(encrypted_message, private_key):
        """Decrypts a message using RSA.

        Returns a UTF-8 string.

        encrypted_message - UTF-8 encoded string (with Base64 representation).
        private_key - rsa.PrivateKey.
        """

        encrypted_b64_bytes = encrypted_message.encode("UTF-8")
        encrypted_bytes = base64.b64decode(encrypted_b64_bytes)
        decrypted_bytes = rsa.decrypt(encrypted_bytes, private_key)
        decrypted_string = decrypted_bytes.decode("UTF-8")

        return decrypted_string


    # function to receive messages

    def receive(self):
        while True:
            try:
                # decrypt message 
                message=self.decrypt(message,private_key)

                # message = client.recv(1024).decode(FORMAT)

                # if the messages from the server is NAME send the client's name
                if message == 'NAME':
                    name=self.encrypt(self.name,public_key)
                    # need destination IP address 
                    SimpleApp.send(name,self.dest)
                else:
                    # insert messages to text box
                    self.textCons.config(state=NORMAL)
                    self.textCons.insert(END,
                                         message+"\n\n")
                    self.textCons.config(state=DISABLED)
                    self.textCons.see(END)

            except:
                # an error will be printed on the command line or console if there's an error
                print("An error occurred!")
                # client.close()
                break
 
    # function to send messages
    def sendMessage(self):
        self.textCons.config(state=DISABLED)
        while True:
            message = (f"{self.encrypt(self.name,public_key)}: {self.encrypt(self.msg,public_key)}")
            SimpleApp.send(message,self.dest)
            break
    

g = GUI()

# To Do List:
# need to redo sendMessage and receive functions with integration with layer4 
# need to come up w/ encryption scheme of data (rsa not working)
# fix receive function 


# questions
# what is the destination IP (integers from layer 3)
# why isn't rsa import working 
