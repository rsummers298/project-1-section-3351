# This is a stub for Layer 2.

# Import Layers
from curses.ascii import isdigit
import sys
import hashlib

sys.path.append('../')
import layer1.mocklayer1 as Layer1
import logging

def divide_chunks(l, n): #divides a list of things into n groups of lists of things
    for i in range(0, len(l), n):
        yield l[i:i + n]
        
def hashData(data): #computes a hash using UTF-8
    hash = hashlib.sha1(data.encode("UTF-8")).hexdigest()
    return hash

class StubLayer2:
    global preamble 
    preamble = "10101011" #preamble to be used to verify messages sent
    def __init__(self, layer_3_cb):
        """Create a Layer 2 Object.

        `layer_3_cb` - Function. This layer will use `layer_3_cb` to pass data to Layer 3. `layer_3_cb`
                       must accept a single parameter `data`.
        """
        # Save parameter inputs as instance attributes so they can be referred to later
        self.layer_3_cb = layer_3_cb

        # Creates a list of multiple interfaces to connect to
        self.layer1_interface_1 = Layer1.MockLayer1(interface_number=1, layer_2_cb=self.from_layer_1, enforce_binary=True)
        self.layer1_interface_2 = Layer1.MockLayer1(interface_number=2, layer_2_cb=self.from_layer_1, enforce_binary=True)
        self.layer1_interface_3 = Layer1.MockLayer1(interface_number=3, layer_2_cb=self.from_layer_1, enforce_binary=True)
        self.interfaceList = [
            self.layer1_interface_1,
            self.layer1_interface_2,
            self.layer1_interface_3,
        ]

    def from_layer_3(self, data, interface):
        """Call this function to send data to this layer from layer 3"""
        hashed = str(hashData(data)) #hash the input
        newData = hashed[:8] + data #add first 8 chars of the hash to the front of the data
        newData="".join(format(ord(x), '08b') for x in newData) #convert data to binary
        newData = preamble + newData #add preamble
        self.interfaceList[interface].from_layer_2(newData) #send binary data to selected interface

    def from_layer_1(self, data):
        """Call this function to send data to this layer from layer 1"""
        charList = [str(x) for x in data] #convert ints in list to strings
        pre=""
        for x in range(0,8): #append pre to compare to preamble
            pre+=charList[0]
            charList.pop(0) #remove preamble as we iterate
        if not (pre==preamble): #test if preamble had bit flips
            return
        byteList = list(divide_chunks(charList, 8)) #divides charList into several lists of length 8
        bytes = []
        counter = 0
        for x in byteList: 
            bytes.append("".join(byteList[counter])) #combines each list into a single string or byte
            bytes[counter] = chr(int(bytes[counter], 2)) #converts the byte to plaintext
            counter+=1
        newData = "".join(bytes) #combines each plaintext character to a single string
        hashed = newData[0:8] #retireves the hash value
        retData = newData[8:] #retrieves the message
        tempData = hashData(retData) #hashes the message to test with the hash value
        tempData = str(tempData)
        if(hashed==tempData[:8]): #if the new and old hashes match, send the message up to layer 3
            self.layer_3_cb(retData)
        else: #if the new and old hashes don't match, do nothing
            return

