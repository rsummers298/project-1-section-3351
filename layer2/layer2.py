"""# Import Layers
import sys
sys.path.append('../')
import mocklayer1 as Layer1
import logging

class StubLayer2:
    def __init__(self, layer_3_cb):
      Create a Layer 2 Object.

        `layer_3_cb` - Function. This layer will use `layer_3_cb` to pass data to Layer 3. `layer_3_cb`
                       must accept a single parameter `data`.
        
        # Save parameter inputs as instance attributes so they can be referred to later
        self.layer_3_cb = layer_3_cb

        # Connect to Layer 1 on interface 1. Your actual implementation will
        # have multiple interfaces.        
        self.layer1_interface_1 = Layer1.MockLayer1(interface_number=1, layer_2_cb=self.from_layer_1)
        self.layer1_interface_2 = Layer1.MockLayer1(interface_number=2, layer_2_cb=self.from_layer_1)
        self.layer1_interface_3 = Layer1.MockLayer1(interface_number=3, layer_2_cb=self.from_layer_1)
        self.layer_interfaces = {1: self.layer1_interface_1, 2: self.layer1_interface_2, 3: self.layer1_interface_3}


    def from_layer_3(self, data, interface):
        """Call this function to send data to this layer from layer 3"""
        logging.debug(f"Layer 2 received msg from Layer 3: {data}")
        
        # Transform data received from layer 3
        data_bits = bin(int.from_bytes(data.encode(), "big"))[2:] 
        
        # need extra 0's to deal with the bin() conversion
        while (len(data_bits)%8 != 0):
            data_bits = '0' + data_bits

        # calculates parity
        p = self.parity(data_bits)

        # header + footer to know where layer 2 ends
        footer = '010101010101' # 12 bits
        header = '101010101010' # 12 bits

        # makes string to send to layer 1
        data = header + p + data_bits  + footer

        # Layer 3 is making the decision about which interface to send the data. Layer 2
        # simply follows Layer 3's directions (and receives the interface number as an
        # argument.
        datalist = list(data)
        
        # Pass the message down to Layer 1. This 
        self.layer_interfaces[interface].from_layer_2(datalist)

    def from_layer_1(self, data):
        """Call this function to send data to this layer from layer 1"""
        logging.debug(f"Layer 2 received msg from Layer 1: {data}")

        logging.debug(f"Layer 2 received msg from Layer 1: {data}")

        # make data stream
        data_str = "".join(data)

        # header + footer to know where layer 2 ends
        footer = '010101010101' # 12 bits
        header = '101010101010' # 12 bits
        
        data_header = data_str[:12]
        parity_recv = data_str[12:21]
        data_bits = data_str[21:(len(data_str)-12)]
        data_footer = data_str[-12:]    
        parity_calc = self.parity(data_bits)

        if (parity_calc != parity_recv):
            print("<PARITY ERROR>, dropping packet")
            return
        if(data_header != header):
            print("<HEADER ERROR> , dropping packet")
            return
        if (data_footer != footer):
            print("<FOOTER ERROR> , dropping packet")
            return

        bytes_num = int(len(data_bits)/8) # get number of bytes
        data = str(int(data_bits, 2).to_bytes(bytes_num, 'big')) # convert to string
        data_bits = bin(int.from_bytes(data.encode(), 'big'))[2:]

        # Let's just forward this up to layer 3.
        self.layer_3_cb(data)

    # takes a message(in bits) with headers and does a parity check, returning 9 bits of the following format:
    # x x x x x x x x x
    # ^ |             |
    # |  \           /
    # |    ---------
    # |     each bit is comparison of an index in each byte of data
    # |
    # whole bitstream check
    #
    # Ex:
    #   bitstream: 111100000110010000100110
    #   total parity bit = 0
    #
    #                   1 | 1 | 1 | 1 | 0 | 0 | 0 | 0
    #                   0 | 1 | 1 | 0 | 0 | 1 | 0 | 0
    #                   0 | 0 | 1 | 0 | 0 | 1 | 1 | 0
    #                  -------------------------------
    # parity by column: 1   0   1   1   0   0   1   0
    # result = 0 1 0 1 1 0 0 1 0  
    def parity(self, msg):
        parity_bits = ""

        #pad msg
        while(len(msg) % 8 != 0):
            msg += '0'

        # get first bit from whole frame
        count = 0
        for i in range(0, len(msg)):
            if msg[i] == 1:
                count += 1
        count %= 2
        if count == 0:
            parity_bits += '0'
        else:
            parity_bits += '1'
        
        # get all bytes in 2d array
        # byte_split = [[0 for x in range(len(msg)/8)], 8]
        
        # splits into bytes
        byte_array = []
        num_bytes = int(len(msg)/8)
        for x in range(num_bytes):
            i = x*8
            print(msg[i:i+8])
            byte_array.append(msg[i:i+8])
        
        print(byte_array)
        parity_arr = []
        #parity for each column
        for x in range(8):
            pcount = 0
            for i in range(len(byte_array)):
                if (byte_array[i][x] == 1):
                    pcount += 1
            if (pcount % 2 == 0):
                parity_bits += '0'
            else: 
                parity_bits += '1'

        return parity_bits

"""
